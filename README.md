# 0xQUEUE

A generic fixed size queue implemented using `void*`.

### API

- Creating the queue:

```c
zer0_queue queue;
zer0_queue_init(&queue, 4, sizeof (int));
//...
zer0_queue_free(&queue);
```

- Adding an item to the queue:

```c
int x0;
x0 = 0xA;

zer0_queue_enqueue(&queue, (void*)&x0, sizeof (int));
```

- Getting the item from queue (returns `NULL` if the queue is empty):

```c
void *v0;
v0 = zer0_queue_dequeue(&queue, sizeof (int));

int v;
if (v0 != NULL)
        v = *(int*)v0;
```

- Getting the item from queue without removing it (returns `NULL` if the queue is empty):

```c
void *v0;
v0 = zer0_queue_peek(&queue, sizeof (int));

int v;
if (v0 != NULL)
        v = *(int*)v0;
```

- Clearing the queue:

```c
zer0_queue_clear(&queue);
```

- Checking if the queue is empty:

```c
bool is_empty;
is_empty = zer0_queue_is_empty(&queue);
```

- Checking if the queue is full:

```c
bool is_full;
is_full = zer0_queue_is_full(&queue);
```
