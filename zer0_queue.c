#include "zer0_queue.h"

#include <malloc.h>
#include <string.h>

#ifdef DEBUG
#include <stdio.h>
#endif

void
zer0_queue_init(zer0_queue *queue, int32_t cap, size_t size) {
	queue->head = -1;
	queue->tail = -1;
	queue->cap  = cap;
	queue->data = malloc((size_t)cap * size);
}

void*
zer0_queue_peek(zer0_queue *queue, size_t size) {
	if (queue->head == -1)
		return NULL;

	void *ptr;
	ptr = queue->data + (size_t)queue->head * size;

	return ptr;
}

void
zer0_queue_enqueue(zer0_queue *queue, void *item, size_t size) {
	if ((queue->head == 0 && queue->tail == queue->cap - 1) ||
	    (queue->head == queue->tail + 1)) {
#ifdef DEBUG
		printf("ZER0 QUEUE [DEBUG]: FULL\n");
#endif
		return;
	}

	if (queue->head == -1)
		queue->head = 0;

	queue->tail = (queue->tail + 1) % queue->cap;

	void *ptr;
	ptr = queue->data + (size_t)queue->tail * size;

	memcpy(ptr, item, size);

#ifdef DEBUG
	printf("ZER0 QUEUE [DEBUG]: ENQ\n");
#endif
}

void*
zer0_queue_dequeue(zer0_queue *queue, size_t size) {
	if (queue->head == -1) {
#ifdef DEBUG
		printf("ZER0 QUEUE [DEBUG]: EMPTY\n");
#endif
		return NULL;
	}

	void *ptr;
	ptr = queue->data + queue->head * size;

	if (queue->head == queue->tail) {
		queue->head = -1;
		queue->tail = -1;
	} else  {
		queue->head = (queue->head + 1) % queue->cap;
	}

#ifdef DEBUG
	printf("ZER0 QUEUE [DEBUG]: DEQ\n");
#endif

	return ptr;
}

bool
zer0_queue_is_full(zer0_queue *queue) {
	if ((queue->head == 0 && queue->tail == queue->cap - 1) ||
	    (queue->head == queue->tail + 1))
		return true;

	return false;
}

bool
zer0_queue_is_empty(zer0_queue *queue) {
	if (queue->head == -1)
		return true;

	return false;
}

void
zer0_queue_clear(zer0_queue *queue) {
	queue->head = -1;
	queue->tail = -1;
}

void
zer0_queue_free(zer0_queue *queue) {
	free(queue->data);
}
