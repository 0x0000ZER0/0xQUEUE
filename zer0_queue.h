#ifndef ZER0_QUEUE_H
#define ZER0_QUEUE_H

#include <stdint.h>
#include <stdbool.h>

typedef struct {
	int32_t  head;
	int32_t  tail;
	int32_t  cap;
	void    *data;
} zer0_queue;

void
zer0_queue_init(zer0_queue*, int32_t, size_t);

void*
zer0_queue_peek(zer0_queue*, size_t);

void
zer0_queue_enqueue(zer0_queue*, void*, size_t);

void*
zer0_queue_dequeue(zer0_queue*, size_t);

bool
zer0_queue_is_full(zer0_queue*);

bool
zer0_queue_is_empty(zer0_queue*);

void
zer0_queue_clear(zer0_queue*);

void
zer0_queue_free(zer0_queue*);

#endif
